class ApplicationController < ActionController::Base
  
  protect_from_forgery with: :exception

  before_filter :authorize

 
  protected

   def authorize
	 unless User.find_by_id(session[:user_id])
	 redirect_to login_url, notice: "Please log in"
	 end
   end

     def current_ticket  
  	Ticket.find(session[:ticket_id])
  rescue ActiveRecord::RecordNotFound
  	ticket=Ticket.create
  	session[:ticket_id]=ticket.id
  	ticket
  end

end
