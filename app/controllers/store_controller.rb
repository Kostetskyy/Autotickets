class StoreController < ApplicationController
  
  	skip_before_filter :authorize
  
  def index
  	@tickets = Ticket.order(:place)
  end
end
