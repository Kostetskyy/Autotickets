class Ticket < ApplicationRecord

	
	has_many :line_items

	has_many :orders, through: :line_items


	validates :count, :place, :arrive, :departure, :image_url, presence: true

	validates :price, numericality: {greater_than_or_equal_to: 0.01}

	validates :image_url, allow_blank: true, format: {
		with: %r{\.(gif|jpg|png)}i,
		message: ' must be a URL for GIF, JPG or PNG image.'
	}
end
