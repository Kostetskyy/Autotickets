class Order < ApplicationRecord

	has_many :tickets
	
	validates :name, :city, :email, presence: true

	def add_line_items_from_ticket(ticket)
		ticket.line_items.each do |item|
			item.ticket_id = nil
			line_items << item
		end
	end
end
