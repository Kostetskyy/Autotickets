json.extract! ticket, :id, :count, :place, :arrive, :departure, :image_url, :price, :created_at, :updated_at
json.url ticket_url(ticket, format: :json)
