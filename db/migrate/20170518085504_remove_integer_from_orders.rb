class RemoveIntegerFromOrders < ActiveRecord::Migration[5.0]
  def up
    remove_column :orders, :integer
  end

  def down
    add_column :orders, :integer
  end
end
