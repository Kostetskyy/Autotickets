class DeleteProductsIdFromLineItems < ActiveRecord::Migration[5.0]
  def up
    remove_column :line_items, :product_id
  end

  def down
    add_column :line_items, :product_id
  end
end
