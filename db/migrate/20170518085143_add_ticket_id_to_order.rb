class AddTicketIdToOrder < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :ticket_id, :string
    add_column :orders, :integer, :string
  end
end
