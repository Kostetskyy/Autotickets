class CreateTickets < ActiveRecord::Migration[5.0]
  def change
    create_table :tickets do |t|
      t.integer :count
      t.string :place
      t.string :arrive
      t.string :departure
      t.string :image_url
      t.decimal :price

      t.timestamps
    end
  end
end
