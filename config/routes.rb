Rails.application.routes.draw do
  
  resources :line_items
  get 'admin' => 'admin#index'
   controller :sessions do
   get 'login' => :new
   post 'login' => :create
   delete 'logout' => :destroy
   end

  resources :users
  resources :orders
  get 'store/index'

  resources :tickets

  root to: 'store#index', as: 'store'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
