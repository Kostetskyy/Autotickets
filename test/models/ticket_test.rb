require 'test_helper'

class TicketTest < ActiveSupport::TestCase

	fixtures :tickets

  test "ticket attributes must not be empty" do
	
	ticket = Ticket.new
		
		assert ticket.invalid?
		assert ticket.errors[:count].any?
		assert ticket.errors[:place].any?
		assert ticket.errors[:arrive].any?
		assert ticket.errors[:departure].any?
		assert ticket.errors[:image_url].any?
		assert ticket.errors[:price].any?
  end

  test "ticket price must be positive" do

		ticket = Ticket.new(count: 16,
		place: "Lviv - Kiev" ,
		arrive: "29.06.2017 15:00 " ,
		departure: "29.06.2017 15:00 " ,
		image_url: "Lorem.jpg" ,
		price: 20.50)
		ticket.price = -1
		assert ticket.invalid?
		assert_equal "must be greater than or equal to 0.01",
		ticket.errors[:price].join('; ')

		ticket.price = 0
		assert ticket.invalid?
		assert_equal "must be greater than or equal to 0.01",
		ticket.errors[:price].join('; ')
		ticket.price = 1
		assert ticket.valid?
  end

  def new_ticket(image_url)
	Ticket.new( count: 16,
		place: "Lviv - Kiev" ,
		arrive: "29.06.2017 15:00 " ,
		departure: "29.06.2017 15:00 " ,
		image_url: image_url ,
		price: 20.50)
  end
	
	test "image url" do
	
		ok = %w{ fred.gif fred.jpg fred.png FRED.JPG FRED.Jpg
		http://a.b.c/x/y/z/fred.gif }
		bad = %w{ fred.doc fred.gif/more fred.gif.more }
		
		ok.each do |name|
		assert new_ticket(name).valid?, "#{name} shouldn't be invalid"
		end
		
		bad.each do |name|
		assert new_ticket(name).invalid?, "#{name} shouldn't be valid"
		end
	end

end
